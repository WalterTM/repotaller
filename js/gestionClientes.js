var clientesObtenidos;

function gestionClientes(){
var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
var request = new XMLHttpRequest();
request.onreadystatechange = function(){
  if(this.readyState == 4 && this.status == 200){
    //console.log(request.responseText);
    clientesObtenidos = request.responseText;
    procesarClientes();
  }
}
  request.open("GET",url,true);
  request.send();
}


function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divClientes = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");//para pintar 1 raya blanca y la otra gris


  for(var i=0;i<JSONClientes.value.length;i++){

    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaPais = document.createElement("td");
    columnaPais.innerText = JSONClientes.value[i].Country;

    var banderaPais = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if(JSONClientes.value[i].Country == 'UK') {
    imgBandera.src = rutaBandera+"United-Kingdom.png";
  }
    else{
    imgBandera.src = rutaBandera+JSONClientes.value[i].Country+".png";
    }
    
    banderaPais.appendChild(imgBandera);


    //var columnaStock = document.createElement("td");
    //columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(banderaPais);
    //nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila)

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
  }
}
